
## Appointments application integrated with Zoom Websdk and API

#### - Installation.

First, run composer install and npm

	composer install && npm install && npm run dev

Then migrate the database

	php artisan migrate
	
Then seed the database

	php artisan db:seed


#### You Can host the meeting by that route if authorized

/meetings/{meeting_id}/host

#### You Can join the meeting by that route if authorized

/meetings/{meeting_id}/join


# !!!----     Thank You ----!!!