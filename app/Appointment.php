<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Appointment extends Model
{

    protected $appends = ['status'];

    protected $fillable = [
        'title', 'date', 'key', 'user_id', 'reserve_id', 'start_time', 'end_time'
    ];
    public function scopeUpcoming($query)
    {
        return $query->whereDate('date', '>=', date('Y-m-d'));
    }

    public function host()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'appointments_users');
    }

    public function meeting(){
        return $this->hasOne(Meeting::class,'appointment_id');
    }

    public function getDateAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d-m-Y');
    }

    public function getStartTimeAttribute($start_time)
    {
        return Carbon::createFromFormat('H:i:s', $start_time)->format('H:i');
    }

    public function getEndTimeAttribute($end_time)
    {
        return Carbon::createFromFormat('H:i:s', $end_time)->format('H:i');

    }

    public function getStatusAttribute()
    {
        $today = Carbon::now();
        $diffInDays = $today->diffInDays(Carbon::createFromFormat('d-m-Y', $this->date), false);
        if ($diffInDays < 0)
            return 0;
        if ($diffInDays == 0) {
            $diffEndTime = $today->diffInSeconds(Carbon::createFromFormat('d-m-Y H:i', $this->date . " " . $this->end_time), false);
            $diffStartTime = $today->diffInSeconds(Carbon::createFromFormat('d-m-Y H:i', $this->date . " " . $this->start_time), false);
            // If same day and upcoming
            if ($diffEndTime > 0 && $diffStartTime < 0)
                return 2;
            // If same day and passed
            if ($diffEndTime < 0)
                return 0;
            //running now
            return 1;
        }
        return 1;

    }
}
