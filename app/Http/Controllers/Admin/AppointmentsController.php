<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Events\InitMeeting;
use App\Http\Controllers\Controller;
use App\Http\Requests\AppointmentRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class AppointmentsController extends Controller
{
    public $data = array();

    public function index(Request $request)
    {
        $this->data['date'] = $request->get('date') ?? null;
        $this->data['start_time'] = $request->get('start_time') ?? null;
        $this->data['end_time'] = $request->get('end_time') ?? null;
        $this->data['host'] = $request->get('host') ?? null;

        $query = Appointment::orderBy('date', 'desc');

        if ($this->data['date']) {
            $query->whereDate('date', '=', Carbon::createFromFormat('d-m-Y', $this->data['date'])->format('Y-m-d'));
        }

        if ($this->data['start_time']) {
            $query->whereTime('start_time', '=', Carbon::createFromFormat('H:i', $this->data['start_time'])->format('h:i:s'));
        }

        if ($this->data['end_time']) {
            $query->whereTime('end_time', '=', Carbon::createFromFormat('H:i', $this->data['end_time'])->format('h:i:s'));
        }

        if ($host = $this->data['host']) {
            $query->whereHas('host', function ($q) use ($host) {
                return $q->where('email', $this->data['host']);
            });
        }
        $this->data['appointments'] = $query->paginate(10);

        return view('admin.appointments.index', $this->data);
    }

    public function show($id)
    {
        $this->data['appointment'] = Appointment::findOrFail($id);
        return view('admin.appointments.show', $this->data);
    }

    public function create()
    {
        $this->data['appointment'] = null;
        $this->data['hosts'] = User::hosts()->get();
        return view('admin.appointments.show', $this->data);
    }

    public function store(AppointmentRequest $request)
    {
        $request->merge(['date' => Carbon::createFromFormat('d-m-Y', $request->get('date'))->format('Y-m-d')]);
        $appointment = Appointment::create($request->all());
        session()->flash('status', 'Appointment Created Successfully');
        if ($appointment->user_id)
            event(new InitMeeting($appointment));
        return redirect()->route('admin.appointments.show', $appointment->id);
    }

    public function update($id, AppointmentRequest $request)
    {
        $request->merge(['date' => Carbon::createFromFormat('d-m-Y', $request->get('date'))->format('Y-m-d')]);
        $appointment = Appointment::find($id);
        $appointment->update($request->all());
        session()->flash('status', 'Appointment Updated Successfully');
        if ($appointment->user_id)
            event(new InitMeeting($appointment));
        return redirect()->route('admin.appointments.show', $appointment->id);
    }

    public function reserve($id, Request $request)
    {
        $appointment = Appointment::findOrFail($id);
        $appointment->users()->syncWithoutDetaching(Auth::id());
        session()->flash('status', 'Seat Reserved Successfully');
        if ($request->get('attend'))
            session()->flash('join', true);
        return redirect()->route('admin.appointments.show', $appointment->id);
    }

    public function cancel($id)
    {
        $appointment = Appointment::findOrFail($id);
        $appointment->users()->detach(Auth::id());
        session()->flash('status', 'Seat Cancelled Successfully');
        return redirect()->route('admin.appointments.show', $appointment->id);
    }


}
