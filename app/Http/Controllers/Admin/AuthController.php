<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        if ($request->method() == "POST") {
            $error = new MessageBag();
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }
            if (!Auth()->attempt([
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            ], !!$request->get('remember_me', 0))) {
                $error->add('invalid', "Incorrect Email or Password");
                return redirect()->back()->withErrors($error->messages())->withInput($request->all());
            }
            return redirect()->route('admin.index');
        }
        return view('admin.login');
    }

    public function register(Request $request)
    {
        if ($request->method() == "POST") {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
                'first_name' => 'required|max:255',
                'last_name' => 'required|max:255',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }
            $user = new User();
            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            $user->role = "attendee";
            $user->password = $request->get('password');
            $user->save();
            Auth::login($user);
            return redirect()->route('admin.index');
        }
        return view('admin.register');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('admin.auth.login');
    }
}
