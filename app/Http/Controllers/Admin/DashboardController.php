<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public $data = array();

    public function index()
    {
        $appointments = Appointment::upcoming()->get();

        $appointments = $appointments->filter(function ($value) {
            if (Carbon::createFromFormat('d-m-Y', $value->date)->diffInDays(Carbon::now()->format('d-m-Y')) == 0) {
                return Carbon::now()->diffInMinutes(Carbon::createFromFormat('H:i', $value->start_time), false) > 0;
            }
            return true;
        });

        $this->data['appointments'] = count($appointments);

        return view('admin.dashboard', $this->data);
    }
}
