<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class HostController extends Controller
{

    public $data = array();

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::hosts();
        if ($this->data['query'] = $request->get('q')) {
            $query->where('first_name', 'like', '%' . $this->data['query'] . '%')
                ->orWhere('last_name', 'like', '%' . $this->data['query'] . '%')
                ->orWhere('email', 'like', '%' . $this->data['query'] . '%');
        }
        $this->data['hosts'] = $query->paginate();
        return view('admin.hosts.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['host'] = null;
        return view('admin.hosts.edit', $this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required|min:6'
        ]);
        $request->merge(['role' => 'host']);
        $user = User::create($request->all());
        session()->flash('status', 'Host Created Successfully');
        return redirect()->route('hosts.show', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->data['host'] = User::findOrFail($id);
        return view('admin.hosts.edit', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $request->validate([
            'email' => 'required|unique:users,email,'.$id,
            'first_name' => 'required',
            'last_name' => 'required',
        ]);
        $user->update($request->all());
        session()->flash('status', 'Host Updated Successfully');
        return redirect()->route('hosts.show', $id);
    }


    public function destroy($id)
    {
        //
    }
}
