<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Http\Controllers\Controller;
use App\Meeting;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Http;

class MeetingsController extends Controller
{
    private $apikey;

    private $apisecret;

    private $url;

    private $jwt;

    public function __construct()
    {
        $this->apikey = env('ZOOM_API', 'EGPhutegRzaFF4Oj1NFKxg');
        $this->apisecret = env('ZOOM_SECRET', 'qicIMYQ27PAYNiqATbtwzOhFoiIQyPI0F31K');
        $this->jwt = $this->generateToken();
        $this->url = "https://api.zoom.us/v2/";
    }

    public function createMeeting($data)
    {
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $this->jwt
            ])->post('https://api.zoom.us/v2/users/YbDf6_FZS8OHY53xm23RCA/meetings', [
                "topic" => $data->title,
                "start_time" => Carbon::createFromFormat('d-m-Y', $data->date)->format('Y-m-d') . "T" . $data->start_time . ':00',
                "timezone" => 'Africa/Cairo',
                "duration" => Carbon::createFromFormat('H:i', $data->end_time)->diffInMinutes(Carbon::createFromFormat('H:i', $data->start_time)),
                "settings" => [
                    "host_video" => true,
                    "audio" => "both",
                    "participant_video" => true,
                    "join_before_host" => false,
                ]
            ]);
            $res = $response['id'];
        } catch (\Exception $e) {
            dd("Something Went Wrong while creating Meeting");
        }

        Meeting::create([
            'meeting_id' => $response['id'],
            'host_id' => $data->user_id,
            'appointment_id' => $data->id,
            'start_time' => Carbon::createFromFormat('d-m-Y', $data->date)->format('Y-m-d') . " " . $data->start_time,
            'end_time' => Carbon::createFromFormat('d-m-Y', $data->date)->format('Y-m-d') . " " . $data->end_time
        ]);
    }

    public function updateMeeting(Appointment $data)
    {
        $meeting = Meeting::where('appointment_id', $data->id)->first();
        try {
            $response = Http::withHeaders([
                'Authorization' => 'Bearer ' . $this->jwt
            ])->patch('https://api.zoom.us/v2/meetings/' . $meeting->meeting_id, [
                "topic" => $data->title,
                "start_time" => Carbon::createFromFormat('d-m-Y', $data->date)->format('Y-m-d') . "T" . $data->start_time . ':00',
                "timezone" => 'Africa/Cairo',
                "duration" => Carbon::createFromFormat('H:i', $data->end_time)->diffInMinutes(Carbon::createFromFormat('H:i', $data->start_time)),
            ]);
        } catch (\Exception $e) {
            dd("Something Went Wrong while updating Meeting");
        }

        $meeting->update([
            'host_id' => $data->user_id,
            'start_time' => Carbon::createFromFormat('d-m-Y', $data->date)->format('Y-m-d') . " " . $data->start_time,
            'end_time' => Carbon::createFromFormat('d-m-Y', $data->date)->format('Y-m-d') . " " . $data->end_time
        ]);
    }

    public function generateToken()
    {
        $key = $this->apisecret;
        $payload = array(
            "iss" => $this->apikey,
            "exp" => 149609164000
        );
        return JWT::encode($payload, $key);
    }
}
