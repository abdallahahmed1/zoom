<?php

namespace App\Http\Controllers\Frontend;

use App\Appointment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MeetingsController extends Controller
{
    public $data = array();

    public function handle($id)
    {
        $appointment = Appointment::findOrFail($id);
        $meeting = $this->data['meeting'] = $appointment->meeting;

        $this->check($appointment, $meeting);
        if (!$appointment->users()->where('id', Auth::id())->exists() && $appointment->user_id != Auth::id())
            abort(403);

        $this->data['meeting'] = $meeting;
        $this->data['user_name'] = Auth::user()->first_name . " " . Auth::user()->last_name;
        $this->data['host'] = 0;
        $this->data['title'] = $appointment->title;
        return view('meetings.index', $this->data);
    }


    public function check($appointment, $meeting)
    {
        if ($appointment->status == 0) {
            $this->data['status'] = "Passed Appointment";
            return view('meetings.index', $this->data);
        }
        if ($appointment->status == 1 || !$meeting) {
            $this->data['status'] = "Upcoming Appointment";
            return view('meetings.index', $this->data);
        }
    }
}


