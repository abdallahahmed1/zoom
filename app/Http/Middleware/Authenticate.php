<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{

    protected function unauthenticated($request, array $guards)
    {
        throw new AuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request, $guards)
        );
    }

    protected function redirectTo($request, $guards)
    {
        if (!$request->expectsJson()) {
            if (in_array("backend", $guards))
                return route('admin.auth.login');
            return route('login');
        }

    }
}
