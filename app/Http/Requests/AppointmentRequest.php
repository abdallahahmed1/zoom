<?php

namespace App\Http\Requests;

use App\Appointment;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AppointmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Route::currentRouteName() == 'admin.appointments.store')
            return true;
        return Appointment::where('id', $this->route('id'))->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'date' => 'required|after_or_equal:' . date('d-m-Y'),
            'start_time' => 'required',
            'end_time' => 'required|after:start_time'
        ];
    }
}
