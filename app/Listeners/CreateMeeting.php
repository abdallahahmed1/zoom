<?php

namespace App\Listeners;

use App\Appointment;
use App\Events\InitMeeting;
use App\Http\Controllers\Admin\MeetingsController;
use App\Meeting;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateMeeting
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle(InitMeeting $event)
    {
        $meeting = new MeetingsController();
        if (Meeting::where('appointment_id', $event->appointment->id)->exists())
            $meeting->updateMeeting(Appointment::find($event->appointment->id));
        else
            $meeting->createMeeting(Appointment::find($event->appointment->id));
    }
}
