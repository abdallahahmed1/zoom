<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = ['meeting_id', 'host_id', 'title', 'start_time', 'end_time', 'appointment_id'];
    protected $table = "meetings";
}
