<?php

namespace App\View\Components;

use App\User;
use Carbon\Carbon;
use Illuminate\View\Component;

class AdminShowAppointment extends Component
{
    public $appointment;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.admin-show-appointment');
    }

    public function hosts(){

        return User::where('role', 'host')->get();
    }
}
