<?php

namespace App\View\Components;

use Illuminate\View\Component;

class HostShowAppointment extends Component
{
    public $appointment;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.host-show-appointment');
    }
}
