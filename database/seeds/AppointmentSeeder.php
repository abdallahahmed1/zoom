<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AppointmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $faker = \Faker\Factory::create();
            \App\Appointment::create([
                'title' => $faker->jobTitle,
                'date' => Carbon::create(2020, 5, 1)->addWeeks(rand(1, 20)),
                'start_time' => rand(0,12).":".rand(0,59),
                'end_time' => rand(12,23).":".rand(0,59),
            ]);
        }
    }
}
