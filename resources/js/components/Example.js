import React from 'react';
import ReactDOM from 'react-dom';
import {ZoomMtg} from '@zoomus/websdk';


const Example = (props) => {
    ZoomMtg.preLoadWasm();
    ZoomMtg.prepareJssdk();

    let {meeting_id} = JSON.parse(props.meeting);
    let meetConfig = {
        apiKey: "EGPhutegRzaFF4Oj1NFKxg",
        apiSecret: "qicIMYQ27PAYNiqATbtwzOhFoiIQyPI0F31K",
        meetingNumber: parseInt(meeting_id),
        passWord: "123456",
        role: props.host ? 1 : 0,
        userName: props.user
    };

    ZoomMtg.generateSignature({
        meetingNumber: meetConfig.meetingNumber,
        apiKey: meetConfig.apiKey,
        apiSecret: meetConfig.apiSecret,
        role: meetConfig.role,
        success(res) {
            ZoomMtg.init({
                leaveUrl: document.querySelector('meta[name="base"]').getAttribute('content'),
                success() {
                    ZoomMtg.join(
                        {
                            meetingNumber: meetConfig.meetingNumber,
                            userName: meetConfig.userName,
                            signature: res.result,
                            apiKey: meetConfig.apiKey,
                            passWord: meetConfig.passWord,
                            success() {
                                console.log('join meeting success');
                            },
                            error(res) {
                                console.log(res);
                            }
                        }
                    );
                },
                error(res) {
                    console.log(res);
                }
            });
        }
    });

    return(
        <div></div>
    );
};


if (document.getElementById('example')) {
    const el = document.getElementById('data')
    const props = Object.assign({}, el.dataset)
    ReactDOM.render(<Example {...props} />, document.getElementById('example'));
}