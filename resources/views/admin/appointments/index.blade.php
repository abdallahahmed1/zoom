@extends('admin.layouts.app')
@section('title','Appointments')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @include('admin.layouts.messages')
            <div class="row justify-content-between">
                @can('manage.appointments')
                    <div class="col-md-2">
                        <a href="{{route('admin.appointments.create')}}" class="btn btn-primary">Add New Appointment</a>
                    </div>
                @endcan
                @can('view.hosted.appointments')
                    <div class="col-md-3">
                        <a href="{{route('admin.appointments.index',['host' => \Auth::user()->email])}}"
                           class="btn btn-primary">My Appointments</a>
                    </div>
                @endcan
            </div>
            <form action="{{route('admin.appointments.index')}}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group ">
                            <label class="bmd-label-floating">Date</label>
                            <input autocomplete="off" type="text" class="form-control datepicker" name="date"
                                   value="{{$date}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="bmd-label-floating">Start time</label>
                            <input autocomplete="off" type="text" id="start_time" class="form-control" name="start_time"
                                   value="{{$start_time}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="bmd-label-floating">End time</label>
                            <input type="text" autocomplete="off" id="end_time" class="form-control" name="end_time"
                                   value="{{$end_time}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="bmd-label-floating">Host</label>
                            <input type="email" autocomplete="off" class="form-control" name="host" value="{{$host}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </form>
            @if(count($appointments))
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">Appointments</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead class="">
                                        <th>Title</th>
                                        <th>Date</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Host</th>
                                        <th>Status</th>
                                        </thead>
                                        <tbody>
                                        @foreach($appointments as $appointment)
                                            <tr>
                                                <td>
                                                    <a href="{{route('admin.appointments.show', $appointment->id)}}">
                                                        {{$appointment->title}}
                                                    </a>
                                                </td>
                                                <td>{{$appointment->date}}</td>
                                                <td>{{$appointment->start_time}}</td>
                                                <td>{{$appointment->end_time}}</td>
                                                <td>{{$appointment->host ? $appointment->host->email : ""}}</td>
                                                @php
                                                    $status = $appointment->status;
                                                @endphp
                                                <td>{{$status ? $status == 2 ? "Running Now" : "Up Comming" : "Passed"}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12 text-center">
                        Page
                        {{ $appointments->currentPage() }}
                        Of
                        {{ $appointments->lastPage() }}
                    </div>

                    <div class="col-lg-12 text-center">
                        {{ $appointments->appends(Request::all())->render() }}
                    </div>

                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">No Appointments</h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true
            })
            $('#start_time').timepicker({
                timeFormat: 'H:i'
            });
            $('#end_time').timepicker({
                timeFormat: 'H:i'
            });
            $('.reserve').on('click', function (e) {
                e.preventDefault();
                if (confirm("Are you sure to reverse this Appointment?")) {
                    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                    $.ajax({
                        url: $(this).attr('href'),
                        type: 'POST',
                        data: {
                            user_id: "{{\Illuminate\Support\Facades\Auth::id()}}"
                        }
                    }).done(function (res) {
                        window.location = window.location
                    }).fail(function () {
                        alert('alert their is an error in the request');
                    })
                }
            })
        });
    </script>
@endpush