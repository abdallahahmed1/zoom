@extends('admin.layouts.app')
@section('title','Appointments')
@section('content')
    <div class="content">
        <div class="container-fluid justify-content-center align-items-center">
            @include('admin.layouts.messages')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">
                                {{$appointment ? \Gate::allows('manage.appointments') ? "Update Appointment" : "View Appointment" : "Add New Appointment"}}
                            </h4>
                        </div>
                        <div class="card-body">
                            @if($appointment)
                                @can('manage.appointments')
                                    <x-admin-show-appointment :appointment="$appointment"/>
                                @endcan
                                @if(\Auth::user()->role == "host")
                                    <x-host-show-appointment :appointment="$appointment"/>
                                @elseif(\Auth::user()->role == "attendee")
                                    <x-member-show-appointment :appointment="$appointment"/>
                                @endif
                            @else
                                <form method="POST" action="{{route('admin.appointments.store')}}">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Title</label>
                                                <input name="title" value="{{\Request::old('title', '')}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Date</label>
                                                <input autocomplete="off" type="text"
                                                       class="form-control datepicker"
                                                       value="{{\Request::old('date', '')}}"
                                                       name="date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Start time</label>
                                                <input autocomplete="off" type="text" id="start_time"
                                                       class="form-control"
                                                       value="{{\Request::old('start_time', '')}}"
                                                       name="start_time">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">End time</label>
                                                <input type="text"
                                                       autocomplete="off"
                                                       id="end_time"
                                                       class="form-control"
                                                       name="end_time"
                                                       value="{{\Request::old('end_time', '')}}"
                                                >
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group" id="source">
                                                <label class="bmd-label-floating">Host</label>
                                                <select type="select" name="user_id" class="form-control">
                                                    @foreach($hosts as $host)
                                                        <option value="{{$host->id}}">{{$host->email}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right">Create
                                        Appointment
                                    </button>
                                </form>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#start_time').timepicker({
                timeFormat: 'H:i'
            });
            $('#end_time').timepicker({
                timeFormat: 'H:i'
            });
            $('.datepicker').datepicker({
                format: "dd-mm-yyyy",
                autoclose: true
            })
        });
    </script>
@endpush