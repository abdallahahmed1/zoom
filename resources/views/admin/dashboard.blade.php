@extends('admin.layouts.app')
@section('title','Trips')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-6 col-sm-6">
                    <div class="card card-stats">
                        <div class="card-header card-header-warning card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">content_paste</i>
                            </div>
                            <p class="card-category">Upcoming Appointments</p>
                            <h3 class="card-title">{{$appointments ?? ''}}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection