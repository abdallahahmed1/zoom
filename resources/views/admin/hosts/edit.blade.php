@extends('admin.layouts.app')
@section('title',$host ? 'Update Host' : 'Create New Host')
@section('content')
    <div class="content">
        <div class="container-fluid justify-content-center align-items-center">
            @include('admin.layouts.messages')

            <div class="row">
                <div class="col-md-2">
                    <a href="{{route('hosts.create')}}" class="btn btn-primary">Add New Host</a>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">
                                {{$host ?  "Update Host" : "Add New Host"}}
                            </h4>
                        </div>
                        <div class="card-body">
                            @if($host)
                                <form method="POST" action="{{route('hosts.update', $host->id)}}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="put" id="">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">First Name</label>
                                                <input name="first_name"
                                                       value="{{\Request::old('first_name', $host->first_name)}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">First Name</label>
                                                <input name="last_name"
                                                       value="{{\Request::old('last_name', $host->last_name)}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">First Name</label>
                                                <input name="email" value="{{\Request::old('email', $host->email)}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right">Update Host</button>
                                </form>
                            @else
                                <form method="POST" action="{{route('hosts.store')}}">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">First Name</label>
                                                <input name="first_name"
                                                       value="{{\Request::old('first_name', '')}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Last Name</label>
                                                <input name="last_name"
                                                       value="{{\Request::old('last_name', '')}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Email</label>
                                                <input name="email" value="{{\Request::old('email', '')}}"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Password</label>
                                                <input name="password" minlength="6"
                                                       type="text"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary pull-right">Create Host</button>
                                </form>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
@endpush