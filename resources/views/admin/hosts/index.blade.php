@extends('admin.layouts.app')
@section('title','Hosts')
@section('content')
    <div class="content">
        <div class="container-fluid">
            @include('admin.layouts.messages')
            <div class="row float-right">
                <div class="col-md-2">
                    <a href="{{route('hosts.create')}}" class="btn btn-primary">Add New Host</a>
                </div>
            </div>
            <form action="{{route('hosts.index')}}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="bmd-label-floating">Host</label>
                            <input type="text" autocomplete="off" class="form-control" name="q" value="{{$query}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary">Filter</button>
                    </div>
                </div>
            </form>
            @if(count($hosts))
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">Hosts</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead class="">
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        </thead>
                                        <tbody>
                                        @foreach($hosts as $host)
                                            <tr>
                                                <td>
                                                    <a href="{{route('hosts.show', $host->id)}}">
                                                        {{$host->first_name}}
                                                    </a>
                                                </td>
                                                <td>{{$host->last_name}}</td>
                                                <td>{{$host->email}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-lg-12 text-center">
                        Page
                        {{ $hosts->currentPage() }}
                        Of
                        {{ $hosts->lastPage() }}
                    </div>

                    <div class="col-lg-12 text-center">
                        {{ $hosts->appends(Request::all())->render() }}
                    </div>

                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title ">No Hosts</h4>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
@endpush