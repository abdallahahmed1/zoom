<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Appointments | Dashboard | @yield('title')</title>
    @section('meta')
        <meta name="title" content="Fleet Management | Dashboard">
        <meta name="description" content="Fleet Management Bus bookings">
    @show
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css">
    <link href="//cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css">
    <link href="{{asset('public/admin_assets/css/material-dashboard.css?v=2.1.2')}}" rel="stylesheet"/>
    <link href="{{asset('public/admin_assets/demo/demo.css')}}" rel="stylesheet"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('public/admin_assets/js/core/popper.min.js')}}"></script>
    <script src="{{asset('public/admin_assets/js/core/bootstrap-material-design.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>
    {{--<script src="//cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>--}}

    @stack('styles')
</head>
<body class="">
<div class="wrapper ">
    @include('admin.layouts.sidebar')
    <div class="main-panel">
        @include('admin.layouts.header')
        @yield('content')
    </div>
</div>
@stack('scripts')
</body>
</html>