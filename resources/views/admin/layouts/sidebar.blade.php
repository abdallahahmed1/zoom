<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <div class="logo"><a href="{{route('admin.index')}}" class="simple-text logo-normal">
            Appointments App
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item  {{(Request::is('backend') ? 'active open' : '')}}">
                <a class="nav-link" href="{{route('admin.index')}}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item {{(Request::is('backend/appointments*') ? 'active' : '')}} ">
                <a class="nav-link" href="{{route('admin.appointments.index')}}">
                    <i class="material-icons">content_paste</i>
                    <p>Appointments</p>
                </a>
            </li>
            @can('manage.hosts')
                <li class="nav-item {{(Request::is('backend/hosts*') ? 'active' : '')}} ">
                    <a class="nav-link" href="{{route('hosts.index')}}">
                        <i class="material-icons">person</i>
                        <p>Hosts</p>
                    </a>
                </li>
            @endcan
        </ul>
    </div>
</div>

