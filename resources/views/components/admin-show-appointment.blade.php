<form method="POST" action="{{route('admin.appointments.update', $appointment->id)}}">
    {{csrf_field()}}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Title</label>
                <input name="title" value="{{$appointment->title}}" type="text" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Date</label>
                <input autocomplete="off" type="text" class="form-control datepicker" name="date"
                       value="{{$appointment->date}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="bmd-label-floating">Start time</label>
                <input autocomplete="off" type="text" id="start_time" class="form-control" name="start_time"
                       value="{{$appointment->start_time}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="bmd-label-floating">End time</label>
                <input type="text" autocomplete="off" id="end_time" class="form-control" name="end_time"
                       value="{{$appointment->end_time}}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group" id="source">
                <label class="bmd-label-floating">Host</label>
                <select type="select" name="user_id" class="form-control">
                    @foreach($hosts as $host)
                        <option value="{{$host->id}}"
                                @if($appointment->user_id == $host->id) selected @endif>{{$host->email}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right">Update Appointment</button>
</form>
