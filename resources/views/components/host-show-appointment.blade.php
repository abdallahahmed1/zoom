<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating">Title</label>
            <input name="title" value="{{$appointment->title}}" type="text" class="form-control" disabled>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="bmd-label-floating">Date</label>
            <input autocomplete="off" type="text" class="form-control datepicker" name="date"
                   value="{{$appointment->date}}" disabled>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label class="bmd-label-floating">Start time</label>
            <input autocomplete="off" type="text" id="start_time" class="form-control" name="start_time"
                   value="{{$appointment->start_time}}" disabled>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="bmd-label-floating">End time</label>
            <input type="text" autocomplete="off" id="end_time" class="form-control" name="end_time"
                   value="{{$appointment->end_time}}" disabled>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="bmd-label-floating">Host</label>
            <input type="text" class="form-control" value="{{$appointment->host ? $appointment->host->email : ""}}" disabled>
        </div>
    </div>
</div>
@if($appointment->user_id == \Auth::id() && $appointment->status == 2)
    <button type="submit" class="btn btn-primary pull-right">Host Appointment</button>
@endif
