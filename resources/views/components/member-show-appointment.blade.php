<form id="form" method="POST" action="{{route('admin.appointments.reserve', $appointment->id)}}">
    {{csrf_field()}}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Title</label>
                <input name="title" value="{{$appointment->title}}" type="text" class="form-control" disabled>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="bmd-label-floating">Date</label>
                <input autocomplete="off" type="text" class="form-control datepicker" name="date"
                       value="{{$appointment->date}}" disabled>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="bmd-label-floating">Start time</label>
                <input autocomplete="off" type="text" id="start_time" class="form-control" name="start_time"
                       value="{{$appointment->start_time}}" disabled>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="bmd-label-floating">End time</label>
                <input type="text" autocomplete="off" id="end_time" class="form-control" name="end_time"
                       value="{{$appointment->end_time}}" disabled>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="bmd-label-floating">Host</label>
                <input type="text" class="form-control" value="{{$appointment->host ? $appointment->host->email : ""}}" disabled>
            </div>
        </div>
    </div>
    @php
        $is_member = $appointment->users()->where('id', \Auth::id())->exists();
    @endphp
    @if(!$is_member)
        @if($appointment->status == 2)
            <input type="hidden" name="attend" value="1">
            <button type="submit" class="btn btn-primary pull-right">Book and attend NOW!</button>
        @else
            <button type="submit" class="btn btn-primary pull-right">Book a Seat!</button>
        @endif
    @else
        <button id="cancel" type="submit" class="btn btn-primary pull-right">Cancel Seat</button>
    @endif
</form>
@if($appointment->status == 2 && $is_member)
    <button id="attend_now" type="submit" class="btn btn-primary pull-right">Attend Now!</button>
@endif

@push('scripts')
    <script>
        $(document).ready(function () {
            @if(session('join'))
            window.open("{{route('meetings.join', $appointment->id)}}", '_blank');
            @endif
            $('#cancel').on('click', function (e) {
                e.preventDefault();
                $('#form').attr('action', '{{route('admin.appointments.cancel', $appointment->id)}}');
                $('#form').submit();
            })
            $('#attend_now').on('click', function (e) {
                e.preventDefault();
                window.open("{{route('meetings.join', $appointment->id)}}", '_blank');
            })
        })
    </script>
@endpush
