<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="base" content="{{env('APP_URL')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Appointments | {{$host ? 'Host | ' : ''}} {{$title}}</title>
</head>
<body>
@if($meeting)
    <div class="flex-center position-ref full-height">
        <div id="example">
            <div id="data" data-host="{{$host}}" data-user="{{$user_name}}" data-meeting="{{$meeting}}"></div>
        </div>
    </div>
    <script src="{{ asset('/public/js/app.js') }}"></script>
@else
    <div class="flex-center position-ref full-height">
        {{$status}}
    </div>
@endif
</body>
</html>
