<?php

use App\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::any('/login', ['middleware' => 'guest', 'uses' => 'AuthController@login'])->name('admin.auth.login');
    Route::any('/register', ['middleware' => 'guest', 'uses' => 'AuthController@register'])->name('admin.auth.register');
    Route::post('/logout', ['middleware' => 'auth:backend', 'uses' => 'AuthController@logout'])->name('admin.auth.logout');
});

Route::middleware('auth:backend')->group(function () {
    Route::get('/', 'DashboardController@index')->name('admin.index');
    Route::resource('hosts', 'HostController')->middleware('can:manage.hosts');
    Route::prefix('appointments')->group(function () {
        Route::get('/', 'AppointmentsController@index')->name('admin.appointments.index');
        Route::get('/{id}/show', 'AppointmentsController@show')->name('admin.appointments.show');
        Route::post('/create', 'AppointmentsController@store')->middleware('can:manage.appointments')->name('admin.appointments.store');
        Route::get('/create', 'AppointmentsController@create')->middleware('can:manage.appointments')->name('admin.appointments.create');
        Route::post('/{id}/update', 'AppointmentsController@update')->middleware('can:manage.appointments')->name('admin.appointments.update');
        Route::post('/{id}/reserve', 'AppointmentsController@reserve')->name('admin.appointments.reserve');
        Route::post('/{id}/cancel', 'AppointmentsController@cancel')->name('admin.appointments.cancel');
    });
});